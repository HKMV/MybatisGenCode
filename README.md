### spring-boot mybatis代码生成。（注：不支持自定义模板，如需要源码模板可自行修改）
>  **示例生成的代码，包含生成后代码缩依赖的类[示例生成项目](https://gitee.com/zhanghw/MybatisGenCode/attach_files/download?i=163926&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F04%2FC4%2FPaAvDFuHnTmATwWwAADo6fJPzvU273.rar%3Ftoken%3D3e38fe1599f597c3c3c4b840b820a5d8%26ts%3D1535614266%26attname%3D%25E7%25A4%25BA%25E4%25BE%258B%25E7%2594%259F%25E6%2588%2590%25E7%259A%2584%25E4%25BB%25A3%25E7%25A0%2581.rar)** 
1.  **新建spring-boot项目，示例为gradle构建** 
![项目依赖](https://images.gitee.com/uploads/images/2018/0830/145111_03df02a1_376055.png "step1.png")

依赖片段如下：
```
compile 'org.springframework.boot:spring-boot-starter-web:2.0.4.RELEASE'
compile 'org.mybatis.spring.boot:mybatis-spring-boot-starter:1.3.2'
compile 'com.alibaba:druid-spring-boot-starter:1.1.10'
compile 'mysql:mysql-connector-java:5.1.47'
compile 'com.spring4all:swagger-spring-boot-starter:1.7.1.RELEASE'
compile 'org.apache.commons:commons-lang3:3.8'
compile 'commons-collections:commons-collections:3.2.2'
```

2.  **新建项目配置（ _注：暂时只支持.yml配置_ ）** 
- _此处数据源用于连接数据库，获取数据表信息，以供代码生成。_   
- druid配置请参考：[https://github.com/alibaba/druid/tree/master/druid-spring-boot-starter](https://github.com/alibaba/druid/tree/master/druid-spring-boot-starter)  
- swagger配置请参考：[https://github.com/SpringForAll/spring-boot-starter-swagger](https://github.com/SpringForAll/spring-boot-starter-swagger)
![项目配置文件](https://images.gitee.com/uploads/images/2018/0830/150054_8da6d5b5_376055.png "step2.png")

3.  **安装生成插件** 
- 方式一：idea-> file-> Settings-> Plugins-> Browser repositories... 搜索“mybatis code generator” 安装。
- 方式二：下载([生成插件](http://https://gitee.com/zhanghw/MybatisGenCode/raw/master/genCode.zip)) idea-> file-> Settings-> Plugins-> Install plugin from disk... 安装。  

4.  **生成代码** 
- idea-> Code-> Code Generator 填写相关信息  
![生成代码](https://images.gitee.com/uploads/images/2018/0830/151621_80e5c96c_376055.png "step3.png")
![填写信息](https://images.gitee.com/uploads/images/2018/0830/151930_e330411f_376055.png "step4.png")  


5.  **手动更新项目目录** 
- 如果项目没有自动更新，手动更新即可
![更新项目目录](https://images.gitee.com/uploads/images/2018/0830/151738_5d96823c_376055.png "step5.png")  


6.  **最终效果** 
- 生成代码如下
![最终效果](https://images.gitee.com/uploads/images/2018/0830/151813_78ee304a_376055.png "step6.png")    