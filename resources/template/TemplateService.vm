package ${packageName}.service;

import ${packageName}.crudparams.PageInfo;
import ${packageName}.entity.${className};

import java.util.List;

/**
 * ${description}
 *
 * @author ${author}
 * @date ${date}
 */
public interface ${className}Service {

    ${className} add(${className} record);

    ${className} update(${className} record);

    ${className} findById(${simpleIdType} id);

    ${className} findByUuid(String uuid);

    List<${className}> findAll(PageInfo pageInfo);

    int selectCount(PageInfo pageInfo);

    int batchInsert(List<${className}> list);

    List<${className}> batchQueryByIds(List<${simpleIdType}> ids);

    List<${className}> batchQueryByUuids(List<String> uuids);

    int delete(${simpleIdType} id);

    int delete(String uuid);

    int delete(${className} record);

    int batchDeleteById(List<${simpleIdType}> ids);

    int batchDeleteByUuid(List<String> uuids);

}
