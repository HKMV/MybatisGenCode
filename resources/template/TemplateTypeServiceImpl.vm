package ${packageName}.service.impl;

import ${packageName}.component.IdWoker;
import ${packageName}.crudparams.PageInfo;
import ${packageName}.dao.${className}Mapper;
import ${packageName}.entity.${className};
import ${packageName}.service.${className}Service;
import ${packageName}.util.BeanUtils;
import ${packageName}.util.Constants;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * ${description}
 *
 * @author ${author}
 * @date ${date}
 */
@Service
public class ${className}ServiceImpl implements ${className}Service {

    private static final Logger logger = LoggerFactory.getLogger(${className}ServiceImpl.class);

    @Autowired
    private ${className}Mapper mapper;
    @Autowired
    private IdWoker idWoker;

    @Override
    public ${className} add(${className} record) {
        initBasicVals(record);

        int result = mapper.insert(record);
        if (result > 0) {
            return record;
        }
        return null;
    }

    @Override
    public ${className} update(${className} record) {
        ${simpleIdType} id = record.getId();
        if (id == null) {
            return null;
        }
        ${className} db${className} = mapper.selectByPrimaryKey(id);
        if (db${className} == null) {
            return null;
        }
        BeanUtils.copyProperties(record, db${className});
        db${className}.setUpdateTime(new Date());
        int result = mapper.updateByPrimaryKey(db${className});
        if (result > 0){
            return db${className};
        }
        return null;
    }

    @Override
    public ${className} findById(${simpleIdType} id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public ${className} findByUuid(String uuid) {
        return mapper.findByUuid(uuid);
    }

    @Override
    public List<${className}> findTree(Integer orgId, Integer projectId) {
        List<${className}> result = new ArrayList<>();
        if (projectId != null && projectId != 0) {
            List<${className}> types = mapper.findByProjectIdAndLevel(projectId, 1);
            if (CollectionUtils.isEmpty(types)) {
                return result;
            }
            for (${className} type : types) {
                type.setChildren(findChildren(type.getId()));
            }
            return types;
        }
        if (orgId != null && orgId != 0) {
            List<${className}> types = mapper.findByOrgIdAndLevel(orgId, 1);
            if (CollectionUtils.isEmpty(types)) {
                return result;
            }
            for (${className} type : types) {
                type.setChildren(findChildren(type.getId()));
            }
            return types;
        }
        logger.error("tree 参数有误");
        return result;
    }

    @Override
    public int batchInsert(List<${className}> list) {
        for (${className} bean : list) {
            initBasicVals(record);
        }
        return mapper.batchInsert(list);
    }

    @Override
    public List<${className}> batchQueryByIds(List<${simpleIdType}> ids) {
        return mapper.batchQueryByIds(ids);
    }

    @Override
    public List<${className}> batchQueryByUuids(List<String> uuids) {
        return mapper.batchQueryByUuids(uuids);
    }

    @Override
    public int delete(${simpleIdType} id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int delete(String uuid) {
        return mapper.deleteByUuid(uuid);
    }

    @Override
    public int delete(${className} record) {
        return delete(record.getId());
    }

    @Override
    public int batchDeleteById(List<${simpleIdType}> ids) {
        if (CollectionUtils.isNotEmpty(ids)) {
            return mapper.batchDeleteById(ids);
        }
        return 0;
    }

    @Override
    public int batchDeleteByUuid(List<String> uuids) {
        if (CollectionUtils.isNotEmpty(uuids)) {
            return mapper.batchDeleteByUuid(uuids);
        }
        return 0;
    }

    private void initBasicVals(${className} record){
        Date date = new Date();
        record.setUuid(idWoker.nextStringId());
        record.setCreateTime(date);
        record.setUpdateTime(date);
        record.setStatus(Constants.NOMAL_STATUS);
    }

    private List<${className}> findChildren(Integer parentId){
        List<${className}> parent = mapper.findByParentId(parentId);
        if (CollectionUtils.isEmpty(parent)) {
            return new ArrayList<>();
        }
        for (${className} type : parent) {
            type.setChildren(findChildren(type.getId()));
        }
        return parent;
    }

    private List<${className}> findChildrenList(Integer parentId){
        List<${className}> result = new ArrayList<>();
        List<${className}> children = mapper.findByParentId(parentId);
        if (CollectionUtils.isEmpty(children)) {
            return result;
        }
        result.addAll(children);
        for (${className} type : children) {
            result.addAll(findChildrenList(type.getId()));
        }
        return result;
    }
}