package com.sourcespro.db;

/**
 * genCode
 *
 * @author zhanghaowei
 * @date 2018/6/16
 */
public class DBColumn {
    private String name;
    private String type;
    private String note;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
