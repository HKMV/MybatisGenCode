package com.sourcespro.domain;

/**
 * genCode
 *
 * @author zhanghaowei
 * @date 2018/6/16
 */
public class MapperColumn {

    private String column;
    private String jdbcType;
    private String property;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getJdbcType() {
        return jdbcType;
    }

    public void setJdbcType(String jdbcType) {
        this.jdbcType = jdbcType;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
