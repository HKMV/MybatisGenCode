package com.sourcespro.domain;

/**
 * genCode
 *
 * @author zhanghaowei
 * @date 2018/6/16
 */
public class JavaField {

    private String name;
    /**
     * 首字母大写，用于get`Id`()
     */
    private String UpName;
    private String type;
    private String note;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpName() {
        return UpName;
    }

    public void setUpName(String upName) {
        UpName = upName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
