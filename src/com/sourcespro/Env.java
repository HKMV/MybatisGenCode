package com.sourcespro;

/**
 * genCode
 *
 * @author zhanghaowei
 * @date 2018/6/15
 */
public class Env {

    public static String projectPath;
    public static String resourcesPath;
    public static String packageName = "";
    public static String author;
    public static String moduleName;
    public static String className;
    public static String tableName;
    public static String description;
    public static String idType;
    public static String jdbcType;
    public static String tableAlias;

}
