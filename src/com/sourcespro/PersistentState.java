package com.sourcespro;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * genCode
 *
 * @author zhanghaowei
 * @date 2018/6/9
 */
@State(name = "myGen", storages = {@Storage("myGen.xml")})
public class PersistentState implements PersistentStateComponent<PersistentState.State> {

    private PersistentState() {
    }

    public static PersistentState getInstance(){
        PersistentState service = ServiceManager.getService(PersistentState.class);
        return service;
    }

    @Nullable
    @Override
    public State getState() {
        return myState;
    }

    @Override
    public void loadState(@NotNull State state) {
        myState = state;
    }

    public static class State{
        public String author;
        public String className;
        public String tableName;
        public String moduleName;
        public String packageName;
        public String description;
        public String tableAlias;
    }

    public State myState;

}
