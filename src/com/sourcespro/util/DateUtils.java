package com.sourcespro.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * genCode
 *
 * @author zhanghaowei
 * @date 2018/6/16
 */
public class DateUtils {

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        return formatter.format(currentTime);
    }

}
