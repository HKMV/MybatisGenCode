package com.sourcespro.util;

import com.google.common.base.CaseFormat;

/**
 * genCode
 *
 * System.out.println(CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, "test-data"));//testData
 * System.out.println(CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, "test_data"));//testData
 * System.out.println(CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, "test_data"));//TestData
 *
 * System.out.println(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, "testdata"));//testdata
 * System.out.println(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, "TestData"));//test_data
 * System.out.println(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, "testData"));//test-data
 *
 * @author zhanghaowei
 * @date 2018/6/16
 */
public class FieldUtils {

    /**
     * 下划线转驼峰 user_info => userInfo
     * @param column
     * @return
     */
    public static String camel(String column){
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, column);
    }

    /**
     * 下划线转驼峰 user_info => UserInfo
     * @param column
     * @return
     */
    public static String UpCamel(String column){
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, column);
    }

    /**
     * SQL数据类型到Java数据类型的转换
     * @param sqlType SQL数据类型
     * @return Java数据类型
     */
    public static String type(String sqlType) {
        String newType;
        if (sqlType.equalsIgnoreCase("varchar")) {
            newType = "String";
        } else if (sqlType.equalsIgnoreCase("char")) {
            newType = "String";
        } else if (sqlType.equalsIgnoreCase("blob")) {
            newType = "byte[]";
        } else if (sqlType.equalsIgnoreCase("text")) {
            newType = "String";
        } else if (sqlType.equalsIgnoreCase("longtext")) {
            newType = "String";
        } else if (sqlType.equalsIgnoreCase("integer")) {
            newType = "Integer";
        } else if (sqlType.equalsIgnoreCase("tinyint")) {
            newType = "Byte";
        } else if (sqlType.equalsIgnoreCase("smallint")) {
            newType = "Integer";
        } else if (sqlType.equalsIgnoreCase("mediumint")) {
            newType = "Integer";
        } else if (sqlType.equalsIgnoreCase("bigint")) {
            newType = "Long";
        } else if (sqlType.equalsIgnoreCase("float")) {
            newType = "Float";
        } else if (sqlType.equalsIgnoreCase("double")) {
            newType = "Double";
        } else if (sqlType.equalsIgnoreCase("decimal")) {
            newType = "BigDecimal";
        } else if (sqlType.equalsIgnoreCase("date")) {
            newType = "Date";
        } else if (sqlType.equalsIgnoreCase("time")) {
            newType = "Date";
        } else if (sqlType.equalsIgnoreCase("datetime")) {
            newType = "Date";
        } else if (sqlType.equalsIgnoreCase("timestmp")) {
            newType = "Date";
        } else if (sqlType.equalsIgnoreCase("year")) {
            newType = "Date";
        } else if (sqlType.equalsIgnoreCase("int")) {
            newType = "Integer";
        } else if (sqlType.equalsIgnoreCase("int unsigned")) {
            newType = "Integer";
        } else if (sqlType.equalsIgnoreCase("bigint unsigned")) {
            newType = "Long";
        } else if (sqlType.equalsIgnoreCase("tinyint")) {
            newType = "Byte";
        } else if (sqlType.equalsIgnoreCase("tinyint unsigned")) {
            newType = "Byte";
        } else {
            newType = sqlType;
        }
        return newType;
    }

    /**
     * SQL数据类型到Java数据类型的转换
     * @param sqlType SQL数据类型
     * @return Java数据类型
     */
    public static String jdbcType(String sqlType) {
        String jdbcType;
        if (sqlType.equalsIgnoreCase("varchar")) {
            jdbcType = "VARCHAR";
        } else if (sqlType.equalsIgnoreCase("char")) {
            jdbcType = "CHAR";
        } else if (sqlType.equalsIgnoreCase("blob")) {
            jdbcType = "BLOB";
        } else if (sqlType.equalsIgnoreCase("longtext")) {
            jdbcType = "LONGVARCHAR";
        } else if (sqlType.equalsIgnoreCase("text")) {
            jdbcType = "LONGVARCHAR";
        } else if (sqlType.equalsIgnoreCase("integer")) {
            jdbcType = "INTEGER";
        } else if (sqlType.equalsIgnoreCase("tinyint")) {
            jdbcType = "TINYINT";
        } else if (sqlType.equalsIgnoreCase("smallint")) {
            jdbcType = "SMALLINT";
        } else if (sqlType.equalsIgnoreCase("mediumint")) {
            jdbcType = "MEDIUMINT";
        } else if (sqlType.equalsIgnoreCase("bigint")) {
            jdbcType = "BIGINT";
        } else if (sqlType.equalsIgnoreCase("float")) {
            jdbcType = "FLOAT";
        } else if (sqlType.equalsIgnoreCase("double")) {
            jdbcType = "DOUBLE";
        } else if (sqlType.equalsIgnoreCase("decimal")) {
            jdbcType = "DECIMAL";
        } else if (sqlType.equalsIgnoreCase("date")) {
            jdbcType = "DATE";
        } else if (sqlType.equalsIgnoreCase("time")) {
            jdbcType = "TIME";
        } else if (sqlType.equalsIgnoreCase("datetime")) {
            jdbcType = "TIMESTAMP";
        } else if (sqlType.equalsIgnoreCase("timestmp")) {
            jdbcType = "TIMESTAMP";
        } else if (sqlType.equalsIgnoreCase("year")) {
            jdbcType = "YEAR";
        } else if (sqlType.equalsIgnoreCase("int")) {
            jdbcType = "INTEGER";
        } else if (sqlType.equalsIgnoreCase("int unsigned")) {
            jdbcType = "INTEGER";
        } else if (sqlType.equalsIgnoreCase("bigint unsigned")) {
            jdbcType = "BIGINT";
        } else if (sqlType.equalsIgnoreCase("tinyint")) {
            jdbcType = "TINYINT";
        } else if (sqlType.equalsIgnoreCase("tinyint unsigned")) {
            jdbcType = "TINYINT";
        } else {
            jdbcType = sqlType;
        }
        return jdbcType;
    }

}
