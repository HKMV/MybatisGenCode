package com.sourcespro.util;

import com.sourcespro.Env;
import com.sourcespro.domain.MapperColumn;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * genCode
 *
 * @author zhanghaowei
 * @date 2018/6/16
 */
public class MapperXmlUtils {

    public static String getBaseColumn(List<MapperColumn> list){
        String tableAliasDot = "";
        if (Env.tableAlias != null && !"".equals(Env.tableAlias.trim())) {
            tableAliasDot = Env.tableAlias.trim() + ".";
        }
        List<String> columns = new ArrayList<>();
        int pos = 0;
        int line = 1;
        for (MapperColumn column : list) {
                String col = tableAliasDot + column.getColumn();
                pos += col.length();
                if (pos >= 80 * line){
                    columns.add("\n\t\t" + col);
                    line ++;
                } else {
                    columns.add(col);
                }
        }
        return StringUtils.join(columns, ",");
    }

    public static String getBaseColumnNoId(List<MapperColumn> list){
        List<String> columns = new ArrayList<>();
        int pos = 0;
        int line = 1;
        for (MapperColumn column : list) {
            if (column.getColumn().equalsIgnoreCase("id")){
                continue;
            }
            String col = column.getColumn();
            pos += col.length();
            if (pos >= 80 * line){
                columns.add("\n\t\t" + col);
                line ++;
            } else {
                columns.add(col);
            }
        }
        return StringUtils.join(columns, ",");
    }

    /**
     * insertValue
     * @param list
     * @return
     *      #{uuid,jdbcType=VARCHAR}
     */
    public static String getInsertVals(List<MapperColumn> list){
        List<String> columns = new ArrayList<>();
        int pos = 0;
        int line = 1;
        for (MapperColumn column : list) {
            if (column.getColumn().equalsIgnoreCase("id")){
                continue;
            }
            String property = column.getProperty();
            String jdbcType = column.getJdbcType();
            String valueStr = "#{"+ property +",jdbcType="+ jdbcType +"}";
            pos += valueStr.length();
            if (pos >= 80 * line){
                columns.add("\n\t\t" + valueStr);
                line ++;
            } else {
                columns.add(valueStr);
            }
        }
        return StringUtils.join(columns, ",");
    }

    /**
     * batchInsertValue
     * @param list
     * @return
     *      #{uuid,jdbcType=VARCHAR}
     */
    public static String getBatchInsertVals(List<MapperColumn> list){
        List<String> columns = new ArrayList<>();
        int pos = 0;
        int line = 1;
        for (MapperColumn column : list) {
            if (column.getColumn().equalsIgnoreCase("id")){
                continue;
            }
            String property = column.getProperty();
            String jdbcType = column.getJdbcType();
            String valueStr = "#{item."+ property +",jdbcType="+ jdbcType +"}";
            pos += valueStr.length();
            if (pos >= 80 * line){
                columns.add("\n\t\t" + valueStr);
                line ++;
            } else {
                columns.add(valueStr);
            }
        }
        return StringUtils.join(columns, ",");
    }

    /**
     * updateValue
     * @param list
     * @return
     *      uuid = #{uuid,jdbcType=VARCHAR},
     */
    public static String getUpdateVals(List<MapperColumn> list){
        List<String> columns = new ArrayList<>();
        for (MapperColumn column : list) {
            String col = column.getColumn();
            if (col.equalsIgnoreCase("id")){
                continue;
            }
            String property = column.getProperty();
            String jdbcType = column.getJdbcType();
            String valueStr =  col + " = #{" + property + ",jdbcType=" + jdbcType + "}";
            columns.add("\n\t\t" + valueStr);
        }
        return StringUtils.join(columns, ",");
    }

}
