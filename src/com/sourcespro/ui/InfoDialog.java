package com.sourcespro.ui;

import com.intellij.openapi.ui.Messages;
import com.sourcespro.GenBean;
import com.sourcespro.PersistentState;

import javax.swing.*;
import java.awt.event.*;

public class InfoDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField author;
    private JTextField className;
    private JTextField moduleName;
    private JTextField packageName;
    private JTextField description;
    private JTextField tableName;
    private JCheckBox isType;
    private JCheckBox controllerCheckBox;
    private JCheckBox serviceCheckBox;
    private JCheckBox mapperCheckBox;
    private JCheckBox mapperXmlCheckBox;
    private JCheckBox apiNoteCheckBox;
    private JCheckBox serviceImplCheckBox;
    private JCheckBox entityCheckBox;
    private JCheckBox lombokCheckBox;
    private JTextField tableAlias;
    private DialogCallBack mCallBack;

    private PersistentState persistentState = PersistentState.getInstance();

    public InfoDialog(DialogCallBack callBack) {
        this.mCallBack = callBack;
        //加载存储的数据
        PersistentState.State myState = persistentState.myState;
        if (myState != null) {
            System.out.println("加载配置成功");
            author.setText(myState.author);
            className.setText(myState.className);
            moduleName.setText(myState.moduleName);
            packageName.setText(myState.packageName);
            description.setText(myState.description);
            tableName.setText(myState.tableName);
            tableAlias.setText(myState.tableAlias);
        }else {
            System.out.println("初始化配置");
        }

        setTitle("code gen helper");
        setContentPane(contentPane);
        setModal(false);
        getRootPane().setDefaultButton(buttonOK);
        setSize(500, 400);
        setLocationRelativeTo(null);

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        if (null != mCallBack){
            //存储本地
            PersistentState.State myState = new PersistentState.State();
            myState.author = author.getText().trim();
            myState.className = className.getText().trim();
            myState.moduleName = moduleName.getText().trim();
            myState.packageName = packageName.getText().trim();
            myState.description = description.getText().trim();
            myState.tableName = tableName.getText().trim();
            myState.tableAlias = tableAlias.getText().trim();
            persistentState.loadState(myState);

            if (!"".equals(myState.tableName) && "".equals(myState.tableAlias)) {
                Messages.showInfoMessage("请填写表查询别名", "提示");
                return;
            }

            //callback
            GenBean genBean = new GenBean();
            genBean.setAuthor(myState.author);
            genBean.setClassName(myState.className);
            genBean.setTableName(myState.tableName);
            genBean.setModuleName(myState.moduleName);
            genBean.setPackageName(myState.packageName);
            genBean.setDescription(myState.description);
            genBean.setTableAlias(myState.tableAlias);
            genBean.setType(isType.isSelected());
            genBean.setGenController(controllerCheckBox.isSelected());
            genBean.setGenService(serviceCheckBox.isSelected());
            genBean.setGenMapper(mapperCheckBox.isSelected());
            genBean.setGenMapperXml(mapperXmlCheckBox.isSelected());
            genBean.setGenApiNote(apiNoteCheckBox.isSelected());
            genBean.setGenServiceImpl(serviceImplCheckBox.isSelected());
            genBean.setGenEntity(entityCheckBox.isSelected());
            genBean.setGenLombok(lombokCheckBox.isSelected());
            if (!controllerCheckBox.isSelected() && !apiNoteCheckBox.isSelected()
                    && !serviceCheckBox.isSelected() && !serviceImplCheckBox.isSelected()
                    && !mapperCheckBox.isSelected() &&!mapperXmlCheckBox.isSelected()
                    && !entityCheckBox.isSelected()){
                Messages.showInfoMessage("至少选择一个除lombok的功能", "提示");
                return;
            }
            mCallBack.ok(genBean);
        }
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public interface DialogCallBack{
        void ok(GenBean genBean);
    }
}
