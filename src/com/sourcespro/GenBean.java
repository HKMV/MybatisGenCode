package com.sourcespro;

/**
 * genCode
 *
 * @author zhanghaowei
 * @date 2018/6/14
 */
public class GenBean {

    private String author;
    private String className;
    private String tableName;
    private String moduleName;
    private String packageName;
    private String description;
    private String tableAlias;
    private Boolean isType;
    private Boolean genController;
    private Boolean genService;
    private Boolean genMapper;
    private Boolean genMapperXml;
    private Boolean genApiNote;
    private Boolean genServiceImpl;
    private Boolean genEntity;
    private Boolean genLombok;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public Boolean getType() {
        return isType;
    }

    public void setType(Boolean type) {
        isType = type;
    }

    public Boolean getGenController() {
        return genController;
    }

    public void setGenController(Boolean genController) {
        this.genController = genController;
    }

    public Boolean getGenService() {
        return genService;
    }

    public void setGenService(Boolean genService) {
        this.genService = genService;
    }

    public Boolean getGenMapper() {
        return genMapper;
    }

    public void setGenMapper(Boolean genMapper) {
        this.genMapper = genMapper;
    }

    public Boolean getGenMapperXml() {
        return genMapperXml;
    }

    public void setGenMapperXml(Boolean genMapperXml) {
        this.genMapperXml = genMapperXml;
    }

    public Boolean getGenApiNote() {
        return genApiNote;
    }

    public void setGenApiNote(Boolean genApiNote) {
        this.genApiNote = genApiNote;
    }

    public Boolean getGenServiceImpl() {
        return genServiceImpl;
    }

    public void setGenServiceImpl(Boolean genServiceImpl) {
        this.genServiceImpl = genServiceImpl;
    }

    public Boolean getGenEntity() {
        return genEntity;
    }

    public void setGenEntity(Boolean genEntity) {
        this.genEntity = genEntity;
    }

    public Boolean getGenLombok() {
        return genLombok;
    }

    public void setGenLombok(Boolean genLombok) {
        this.genLombok = genLombok;
    }
}
